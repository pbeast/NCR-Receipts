//
//  AppDelegate.m
//  NCR Receipts
//
//  Created by Pavel Yankelevich on 12/12/14.
//  Copyright (c) 2014 Pavel Yankelevich. All rights reserved.
//

#import "AppDelegate.h"
#import "Parse/Parse.h"
#import "HostViewController.h"
#import "ParseFacebookUtils/PFFacebookUtils.h"
#import "UIApplication+SimulatorRemoteNotifications.h"
#import <ParseUI/ParseUI.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
//    [PFUser enableAutomaticUser];
    
    PFACL *defaultACL = [PFACL ACL];
    // If you would like all objects to be private by default, remove this line.
    [defaultACL setPublicReadAccess:YES];
    
    [PFACL setDefaultACL:defaultACL withAccessForCurrentUser:YES];

    [Parse enableLocalDatastore];

    //Prod
    [Parse setApplicationId:@"GI0onNPgipudQrPflEVJJBQz83Ms9GuuaUXzIawS" clientKey:@"r1ch0MW8clfboCMxFSIkZqsxv8wPTXiU8f1LWAVd"];

    //Dev
    //[Parse setApplicationId:@"SNGu8bRK5nxTwGTh3qrvDzFE8tRbIarDGaRsKxwr" clientKey:@"W3CAAQRO4Xq2TqtDtvRgZKtYaQLrGIZsPnbOKCwp"];

    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];

    [PFImageView class];
    
    [PFTwitterUtils initializeWithConsumerKey:@"mtRTM5d2lVViR2JJ9ZpC75hXg" consumerSecret:@"6HnMg8YC0b8IZbJdl7VvBDDclw86XbP1EZlPIQfDyovvk0tTmn"];
    
    [PFFacebookUtils initializeFacebook];
    
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 80000
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound);
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes categories:nil];
        [application registerUserNotificationSettings:settings];
        [application registerForRemoteNotifications];
    } else
#endif
    {
        [application registerForRemoteNotificationTypes: (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
    }
    
    if (launchOptions != nil)
    {
        NSDictionary* userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        if (userInfo != nil)
        {
            NSLog(@"Launched from push notification: %@", userInfo);
            
//            int *pinCode = [[userInfo objectForKey:@"pinCode"] intValue];
            
            UINavigationController* navController = (UINavigationController*)  self.window.rootViewController;
            HostViewController* hostViewController = (HostViewController*)  [navController.viewControllers firstObject];
            [hostViewController setShouldStartFromReceiptView:YES];
        }
    }
    
#if DEBUG
//    [application listenForRemoteNotifications];
#endif

    return YES;
}

/*
 
 ///////////////////////////////////////////////////////////
 // Uncomment this method if you are using Facebook
 ///////////////////////////////////////////////////////////
 
 - (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation 
 {
    return [PFFacebookUtils handleOpenURL:url];
 }
 */

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication withSession:[PFFacebookUtils session]];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [FBAppCall handleDidBecomeActiveWithSession:[PFFacebookUtils session]];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    [currentInstallation saveInBackground];
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    UINavigationController* navController = (UINavigationController*)  self.window.rootViewController;
    HostViewController* hostViewController = (HostViewController*)  [navController.viewControllers firstObject];
    
    [hostViewController newTicketWithPinCode:[[userInfo objectForKey:@"pinCode"] intValue]];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

//- (void)applicationDidBecomeActive:(UIApplication *)application {
//    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
////    
////    UINavigationController* navController = (UINavigationController*)  self.window.rootViewController;
////    HostViewController* hostViewController = (HostViewController*)  [navController.viewControllers firstObject];
//}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.

    [[PFFacebookUtils session] close];
}

@end
