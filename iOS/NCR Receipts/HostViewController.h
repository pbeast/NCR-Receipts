//
//  HostViewController.h
//  NCR Receipts
//
//  Created by Pavel Yankelevich on 12/15/14.
//  Copyright (c) 2014 Pavel Yankelevich. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HostViewController : UIViewController

-(void)newTicketWithPinCode:(int)pinCode;

@property (nonatomic)BOOL shouldStartFromReceiptView;

@end
