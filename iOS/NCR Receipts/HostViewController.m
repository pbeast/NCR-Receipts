//
//  HostViewController.m
//  NCR Receipts
//
//  Created by Pavel Yankelevich on 12/15/14.
//  Copyright (c) 2014 Pavel Yankelevich. All rights reserved.
//

#import "HostViewController.h"
#import "SyncViewController.h"
#import "ReceiptsViewController.h"
#import "Parse/Parse.h"
#import <ParseUI/ParseUI.h>
#import <SVProgressHUD.h>
#import "ReceiptsTableViewController.h"
#import "ParseFacebookUtils/PFFacebookUtils.h"

@interface HostViewController ()<UIPageViewControllerDataSource, PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate>
{
    SyncViewController* syncViewController;
    ReceiptsTableViewController* receiptsViewController;
    BOOL firstAppearence;
    BOOL inBackground;
}
@property (strong, nonatomic) UIPageViewController *pageViewController;

@end


@implementation HostViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    firstAppearence = YES;
    
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    self.pageViewController.dataSource = self;
    
    syncViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SyncViewController"];
    [self addChildViewController:syncViewController];

    if (receiptsViewController == nil){
        receiptsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReceiptsTableViewController"];
//        receiptsViewController.stackedLayout.layoutMargin = UIEdgeInsetsMake(60.0, 0.0, 0.0, 0.0);;
//        receiptsViewController.exposedLayoutMargin = UIEdgeInsetsMake(60.0, 0.0, 0.0, 0.0);
    }
    
    // Change the size of page view controller
    self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refresh:)];

    PFUser *currentUser = [PFUser currentUser];
    if (currentUser) {
        
            self.navigationItem.title = @"Logging In...";
            [self getSocialData:currentUser];
            self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"exit"] style:UIBarButtonItemStylePlain target:self action:@selector(login:)];
    }
    else
    {
        self.navigationItem.title = @"Anonymous";
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"enter"] style:UIBarButtonItemStylePlain target:self action:@selector(login:)];
//        [PFAnonymousUtils logInWithBlock:^(PFUser *user, NSError *error) {
//            self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"enter"] style:UIBarButtonItemStylePlain target:self action:@selector(login:)];
//        }];
        
        PFInstallation *installation = [PFInstallation currentInstallation];

        [installation saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            ;
        }];
    }
    
    inBackground = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationIsActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
}

- (void)applicationIsActive:(NSNotification *)notification {
    if (inBackground){
        [syncViewController performSelector:@selector(startRefresh:) withObject:self];
        [receiptsViewController performSelector:@selector(startRefresh:) withObject:self];
        inBackground = NO;
    }
}

- (void)applicationDidEnterBackground:(NSNotification *)notification {
    inBackground = YES;
}

-(IBAction)login:(id)sender
{
    PFUser *currentUser = [PFUser currentUser];
    if (currentUser == nil)
    {
        [self showLogin];
    }
    else
    {
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];

        [PFUser logOut];

        [receiptsViewController setShouldLoadReceipts:YES];
        
        self.navigationItem.title = @"Anonymous";
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"enter"] style:UIBarButtonItemStylePlain target:self action:@selector(login:)];

        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"lastFetchTime"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        PFQuery *query = [PFQuery queryWithClassName:@"Receipts"];
        [query fromLocalDatastore];
        [query includeKey:@"retailer"];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
        {
            [PFObject unpinAllInBackground:objects block:^(BOOL succeeded, NSError *error) {
                
                PFInstallation *installation = [PFInstallation currentInstallation];
                [installation removeObjectForKey:@"user"];
                [installation saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                    NSLog(@"User unassociated with installation");
                    
                    [SVProgressHUD dismiss];

                    [syncViewController performSelector:@selector(startRefresh:) withObject:self];
                    [receiptsViewController performSelector:@selector(startRefresh:) withObject:self];
                }];
            }];
        }];
    }
}

- (void)getSocialData:(PFUser *)user
{
    if ([PFFacebookUtils isLinkedWithUser:user])
    {
        self.navigationItem.title = @"Facebook User";
        FBRequest *request = [FBRequest requestForMe];
        [request startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
            if (!error) {
                // result is a dictionary with the user's Facebook data
                NSDictionary *userData = (NSDictionary *)result;
                
//                NSString *facebookID = userData[@"id"];
                NSString *name = userData[@"name"];
                self.navigationItem.title = name;
/*
                NSString *location = userData[@"location"][@"name"];
                NSString *gender = userData[@"gender"];
                NSString *birthday = userData[@"birthday"];
                NSString *relationship = userData[@"relationship_status"];
                
                NSURL *pictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", facebookID]];
                
*/
            }
        }];
    }
    else if ([PFTwitterUtils isLinkedWithUser:user])
    {
        self.navigationItem.title = @"Twitter User";
        NSURL *verify = [NSURL URLWithString:@"https://api.twitter.com/1.1/account/settings.json"];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:verify];
        [[PFTwitterUtils twitter] signRequest:request];
        NSURLResponse *response = nil;
        NSError*error;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        self.navigationItem.title = [NSString stringWithFormat:@"@%@", json[@"screen_name"] ];
    }
    else
        self.navigationItem.title = user.username;
}

- (void)logInViewController:(PFLogInViewController *)controller didLogInUser:(PFUser *)user {
    [self dismissViewControllerAnimated:YES completion:nil];

    [self getSocialData:user];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"exit"] style:UIBarButtonItemStylePlain target:self action:@selector(login:)];

    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"lastFetchTime"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    [syncViewController performSelector:@selector(startRefresh:) withObject:self];
    [receiptsViewController setShouldLoadReceipts:YES];

    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    
    PFInstallation* installation = [PFInstallation currentInstallation];
    
    [PFCloud callFunctionInBackground:@"associateInstallationWithUser" withParameters:@{@"installationId" : [installation installationId]} block:^(NSString *result, NSError *error)
    {
        if (!error) {
            NSLog(@"Installation was associated with user");
        }
        else
            NSLog(@"Failed associate instalation with user");
        
        [SVProgressHUD dismiss];
    }];

    
//    PFInstallation *installation = [PFInstallation currentInstallation];
//    installation[@"user"] = [PFUser currentUser];
//    [installation saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
//        NSLog(@"Installation was associated with user");
//        [SVProgressHUD dismiss];
//
//    }];
}

-(void)logInViewController:(PFLogInViewController *)logInController didFailToLogInWithError:(NSError *)error{
    [SVProgressHUD dismiss];
}

- (void)signUpViewController:(PFSignUpViewController *)signUpController didSignUpUser:(PFUser *)user
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    self.navigationItem.title = user.username;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"exit"] style:UIBarButtonItemStylePlain target:self action:@selector(login:)];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"lastFetchTime"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    [syncViewController performSelector:@selector(startRefresh:) withObject:self];
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];

    [receiptsViewController setShouldLoadReceipts:YES];

    PFInstallation *installation = [PFInstallation currentInstallation];
    [PFCloud callFunctionInBackground:@"associateInstallationWithUser" withParameters:@{@"installationId" : [installation installationId]} block:^(NSString *result, NSError *error)
     {
         if (!error) {
             NSLog(@"Installation was associated with user");
         }
         else
             NSLog(@"Failed associate instalation with user");
         
         [SVProgressHUD dismiss];
     }];
    
//    installation[@"user"] = [PFUser currentUser];
//    [installation saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
//        NSLog(@"Installation was associated with user");
//
//        [SVProgressHUD dismiss];
//    }];
}

- (void)logInViewControllerDidCancelLogIn:(PFLogInViewController *)logInController {
    [SVProgressHUD dismiss];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(BOOL)logInViewController:(PFLogInViewController *)logInController shouldBeginLogInWithUsername:(NSString *)username password:(NSString *)password
{
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    
    return YES;
}

- (void)showLogin
{
    PFLogInViewController *logInController = [[PFLogInViewController alloc] init];
    logInController.delegate = self;
//    logInController.signUpController.delegate = self;
//    
//    logInController.emailAsUsername = YES;
//    logInController.signUpController.emailAsUsername = YES;

    logInController.fields = (
                              PFLogInFieldsUsernameAndPassword |
                              PFLogInFieldsLogInButton |
                              PFLogInFieldsSignUpButton |
                              PFLogInFieldsPasswordForgotten |
                              PFLogInFieldsTwitter
                              | PFLogInFieldsFacebook
                              | PFLogInFieldsDismissButton
                              );
    
//    logInController.facebookPermissions = [[NSArray alloc] initWithObjects:@"basic_info", @"email", @"user_birthday", nil];
    logInController.facebookPermissions = [[NSArray alloc] initWithObjects:@"public_profile", @"user_about_me", @"email", nil];
    
    logInController.view.backgroundColor = [UIColor colorWithRed:78.0 / 255.0 green:151.0 / 255.0 blue:31.0 / 255.0 alpha:1];
    logInController.signUpController.view.backgroundColor = [UIColor colorWithRed:78.0 / 255.0 green:151.0 / 255.0 blue:31.0 / 255.0 alpha:1];
    
    UILabel* loginLogo = [[UILabel alloc] init];
    UILabel* signUplogo = [[UILabel alloc] init];
    
    [loginLogo setTextColor:[UIColor whiteColor]];
    [signUplogo setTextColor:[UIColor whiteColor]];
    
    [loginLogo setText:@"NCR Receipts"];
    [loginLogo setFont:[UIFont fontWithName:@"AmericanTypewriter" size:48]];
    
    [signUplogo setText:@"NCR Receipts"];
    [signUplogo setFont:[UIFont fontWithName:@"AmericanTypewriter" size:48]];
    
    logInController.logInView.logo = loginLogo;
    logInController.signUpController.signUpView.logo = signUplogo;
    
    [self presentViewController:logInController animated:YES completion:nil];
    
    logInController.signUpController.emailAsUsername = YES;
    logInController.signUpController.delegate = self;
    logInController.emailAsUsername = YES;
}

-(IBAction)refresh:(id)sender
{
    UIViewController *currentView = [self.pageViewController.viewControllers objectAtIndex:0];

    [currentView performSelector:@selector(startRefresh:) withObject:self];
}

-(void)viewWillAppear:(BOOL)animated
{
    if (firstAppearence == NO)
        return;
    
    firstAppearence = NO;
    
    NSArray *viewControllers = _shouldStartFromReceiptView ?  @[ receiptsViewController ] : @[ syncViewController ];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
}

-(void)newTicketWithPinCode:(int)pinCode
{
    [syncViewController performSelector:@selector(startRefresh:) withObject:self];

    UIViewController *currentView = [self.pageViewController.viewControllers objectAtIndex:0];
    if ([currentView isKindOfClass:[ReceiptsTableViewController class]])
    {
        ReceiptsTableViewController* rc = (ReceiptsTableViewController*)currentView;
        [rc startRefresh:self];
        
        return;
    }
    
    [receiptsViewController setShouldLoadReceipts:YES];
    NSArray *viewControllers = @[ receiptsViewController ];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    if ([viewController isKindOfClass:[SyncViewController class]])
        return nil;
    
    return syncViewController;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    if ([viewController isKindOfClass:[ReceiptsTableViewController class]])
        return nil;
    
    return receiptsViewController;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
