//
//  ReceiptViewCell.m
//  NCR Receipts
//
//  Created by Pavel Yankelevich on 12/15/14.
//  Copyright (c) 2014 Pavel Yankelevich. All rights reserved.
//

#import "ReceiptViewCell.h"

@implementation ReceiptViewCell

-(void)awakeFromNib{
    UIImage *image = [[UIImage imageNamed:@"Background"] resizableImageWithCapInsets:UIEdgeInsetsMake(10.0, 10.0, 10.0, 10.0)];
    
    self.imageView.image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.imageView.tintColor = [UIColor randomColor];//[UIColor colorWithRed:78.0 / 255.0 green:151.0 / 255.0 blue:31.0 / 255.0 alpha:1];
}

@end


