//
//  ReceiptsViewController.h
//  NCR Receipts
//
//  Created by Pavel Yankelevich on 12/15/14.
//  Copyright (c) 2014 Pavel Yankelevich. All rights reserved.
//

#import "TGLStackedViewController.h"

@interface ReceiptsViewController : TGLStackedViewController

@property (assign, nonatomic) BOOL shouldLoadReceipts;

-(IBAction)startRefresh:(id)sender;

@end
