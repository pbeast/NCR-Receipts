//
//  ReceiptsViewController.m
//  NCR Receipts
//
//  Created by Pavel Yankelevich on 12/15/14.
//  Copyright (c) 2014 Pavel Yankelevich. All rights reserved.
//

#import "ReceiptsViewController.h"
#import "ReceiptViewCell.h"
#import "FullReceiptViewController.h"
#import "SVProgressHUD.h"
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>

@interface ReceiptsViewController ()
{
    NSArray* receipts;
    UIRefreshControl *refreshControl;
    long indexOfTheReceiptToDisplay;
}
@end

@implementation ReceiptsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _shouldLoadReceipts = YES;
    
    self.stackedLayout.fillHeight = YES;
    
    // Set to NO to prevent a small number
    // of cards from being scrollable and
    // bounce
    //
    self.stackedLayout.alwaysBounce = YES;
    
    // Set to NO to prevent unexposed
    // items at top and bottom from
    // being selectable
    //
    self.unexposedItemsAreSelectable = YES;
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(startRefresh:) forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:refreshControl];
    
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
    
    recognizer.delaysTouchesBegan = YES;
    recognizer.numberOfTapsRequired = 2;
    
    [self.collectionView addGestureRecognizer:recognizer];

}

- (IBAction)handleDoubleTap:(UITapGestureRecognizer *)recognizer
{
    NSIndexPath *ip = [self.collectionView indexPathForItemAtPoint:[recognizer locationInView:self.collectionView]];
    indexOfTheReceiptToDisplay = ip.row;
    
    [self performSegueWithIdentifier:@"ShowFullReceipt" sender:self];
}

-(IBAction)startRefresh:(id)sender{
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];

    if ([PFUser currentUser])
    {
        [PFCloud callFunctionInBackground:@"fetchReceiptsByUser" withParameters:@{  } block:^(NSObject *result, NSError *error)
         {
             if (!error) {
                 if ([result isKindOfClass:[NSArray class]]){
                     receipts = [result copy];
                 }
             } 
             else
                 receipts = nil;
             
             [refreshControl endRefreshing];
             
             [SVProgressHUD dismiss];
             
             [self.collectionView reloadData];
         }];
        
        return;
    }
    
    [PFCloud callFunctionInBackground:@"fetchReceiptsByInstallationId" withParameters:@{ @"installationId" : [[PFInstallation currentInstallation] installationId] } block:^(NSObject *result, NSError *error)
     {
         if (!error) {
             if ([result isKindOfClass:[NSArray class]]){
                 receipts = [result copy];
             }
         }
         else
             receipts = nil;
         
         [refreshControl endRefreshing];
         
         [SVProgressHUD dismiss];
         
         [self.collectionView reloadData];
     }];
}

-(void)viewWillAppear:(BOOL)animated
{
//    self.navigationController.navigationBar.hidden = NO;
//    self.stackedLayout.layoutMargin = UIEdgeInsetsMake(60, 0.0, 0.0, 0.0);;
//    self.exposedLayoutMargin = UIEdgeInsetsMake(60, 0.0, 0.0, 0.0);
   
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    if (_shouldLoadReceipts == NO)
        return;
    
    _shouldLoadReceipts = NO;
    
    [self startRefresh:self];
}



#pragma mark - CollectionViewDataSource protocol

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return receipts == nil ? 0 : [receipts count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ReceiptViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ReceiptCell" forIndexPath:indexPath];
    
    PFObject *receipt = [receipts objectAtIndex:indexPath.row];
    
    [cell.receiptView loadHTMLString:receipt[@"receipt"] baseURL:nil];
    
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue destinationViewController] isKindOfClass:[FullReceiptViewController class]])
    {
        self.navigationController.title = @"Back";
        FullReceiptViewController* frc = [segue destinationViewController];
        PFObject *receipt = [receipts objectAtIndex:indexOfTheReceiptToDisplay];
        [frc setReceiptToDisplay:receipt[@"receipt"]];
    }
}


@end
