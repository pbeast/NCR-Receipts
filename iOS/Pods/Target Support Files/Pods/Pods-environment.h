
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// Bolts
#define COCOAPODS_POD_AVAILABLE_Bolts
#define COCOAPODS_VERSION_MAJOR_Bolts 1
#define COCOAPODS_VERSION_MINOR_Bolts 1
#define COCOAPODS_VERSION_PATCH_Bolts 3

// Facebook-iOS-SDK
#define COCOAPODS_POD_AVAILABLE_Facebook_iOS_SDK
#define COCOAPODS_VERSION_MAJOR_Facebook_iOS_SDK 3
#define COCOAPODS_VERSION_MINOR_Facebook_iOS_SDK 21
#define COCOAPODS_VERSION_PATCH_Facebook_iOS_SDK 1

// SVProgressHUD
#define COCOAPODS_POD_AVAILABLE_SVProgressHUD
#define COCOAPODS_VERSION_MAJOR_SVProgressHUD 1
#define COCOAPODS_VERSION_MINOR_SVProgressHUD 1
#define COCOAPODS_VERSION_PATCH_SVProgressHUD 2

// TGLStackedViewController
#define COCOAPODS_POD_AVAILABLE_TGLStackedViewController
#define COCOAPODS_VERSION_MAJOR_TGLStackedViewController 1
#define COCOAPODS_VERSION_MINOR_TGLStackedViewController 0
#define COCOAPODS_VERSION_PATCH_TGLStackedViewController 4

// ZXingObjC
#define COCOAPODS_POD_AVAILABLE_ZXingObjC
#define COCOAPODS_VERSION_MAJOR_ZXingObjC 3
#define COCOAPODS_VERSION_MINOR_ZXingObjC 0
#define COCOAPODS_VERSION_PATCH_ZXingObjC 3

// ZXingObjC/All
#define COCOAPODS_POD_AVAILABLE_ZXingObjC_All
#define COCOAPODS_VERSION_MAJOR_ZXingObjC_All 3
#define COCOAPODS_VERSION_MINOR_ZXingObjC_All 0
#define COCOAPODS_VERSION_PATCH_ZXingObjC_All 3

