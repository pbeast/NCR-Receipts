Parse.Cloud.job("cleanUpReceipts", function(request, status) {
  var moment = require('moment');

  Parse.Cloud.useMasterKey();

  var threshold = new Date(moment().subtract('h', 1).format());
  console.log("cleanUpReceipts threshold: " + threshold);

  var receiptsQuery = new Parse.Query("Receipts");
  receiptsQuery.equalTo("state", 0);
  receiptsQuery.lessThanOrEqualTo("updatedAt", threshold);

  receiptsQuery.each(function(receipt) 
  {
//        status.message("");
	return receipt.destroy();
  }).then(function() {
    // Set the job's success status
    status.success("Receipts cleanUp completed successfully.");
  }, function(error) {
    // Set the job's error status
    status.error("Receipts cleanUp failed: " + error.message);
  });
});

Parse.Cloud.define("registerReceipt", function(request, response) {

	if (request.params.installationId == undefined){
		response.error("installationId is mandatory parameter");
		return
	}

    var Counter = Parse.Object.extend("Counter");
    var queryCounter = new Parse.Query(Counter);

    queryCounter.get("fDEqKghN06", { 
        success: function(object) {
            object.increment('sequence');
            if (object.get('sequence') == 100000)
	            object.increment('sequence', -100000);

            object.save(null,{
                success:function(sequenceObject) 
                {
                	console.log(sequenceObject);
                	console.log(sequenceObject.get("sequence"));

                	var Receipts = Parse.Object.extend("Receipts");
                	var newReceipt = new Receipts();

                	newReceipt.save(
                	{
                		"pinCode" : sequenceObject.get("sequence"),
                		"installationId" : request.params.installationId,
                		"user" : request.user,
                		"state" : 0
                	}, 
                	{
						  success: function(receipt) 
						  {
			                    response.success(receipt);
						  },
						  error: function(receipt, error) {
						    response.error('Failed to create new object, with error code: ' + error.message);
						  }
						});
                },
                error:function(error) { 
		            console.log(error);
                    response.error(error);
                }
            });
        }, error: function (error) { 
            console.log(error);
            response.error(error);
        }
    });
});

Parse.Cloud.define("publishReceipt", function(request, response) 
{
	if (request.params.pinCode == undefined){
		response.error("pinCode is mandatory parameter");
		return
	}

	if (request.params.receipt == undefined){
		response.error("receipt is mandatory parameter");
		return
	}

	if (request.params.total == undefined){
		response.error("total is mandatory parameter");
		return
	}

	if (request.params.retailerId == undefined){
		response.error("retailerId is mandatory parameter");
		return
	}

	if (request.params.storeAddress == undefined){
		response.error("storeAddress is mandatory parameter");
		return
	}

	// response.success(parseInt(request.params.pinCode, 10));
	// return;

	var Retailer = Parse.Object.extend("Retailer");
	var retailerQuery = new Parse.Query(Retailer);
	retailerQuery.get(request.params.retailerId, {
	  success: function(retailer) 
	  {
		var receiptsQuery = new Parse.Query("Receipts");

		var pinCode = parseInt(request.params.pinCode, 10)
		receiptsQuery.equalTo("pinCode", pinCode);
		receiptsQuery.equalTo("state", 0);
		receiptsQuery.first(
		{
		    success: function(result) {

	            result.set("receipt", request.params.receipt);
	            result.set("total", request.params.total);
	            result.set("storeAddress", request.params.storeAddress);
	            result.set("retailer", retailer);
	            result.set("state", 1);
	            result.save(null, 
	            {
	                success:function ()
	                {
	                	var installationId = result.get("installationId");
	                	var query = new Parse.Query(Parse.Installation);
						query.equalTo('installationId', installationId); 
						Parse.Push.send({
	  										where: query,
	  										data: {
	    											alert: "New Receipt is ready",
	    											title: "NCR Receipts",
	    											badge: "1",
	    											pinCode: pinCode
	  										}
										}, 
										{
						  success: function() {
						    console.log("Push was successful");
						  },
						  error: function(error) {
						    console.log("Push was unsuccessful: " + error.message);
						  }
						});
				        response.success("Receipt published successfully");
	                },
	                error:function (error)
	                {
	                    response.error("Failed to publish receipt. Error=" + error.message);
	                }
	            });
		    },
		    error: function() {
		        response.error("receipt record lookup failed");
		    }
		});
	  },
	  error: function(object, error) {
		response.error("Retailer not found. Error=" + error.message);
	  }
	});
});


Parse.Cloud.define("fetchReceiptByPinCode", function(request, response) {
	if (request.params.pinCode == undefined){
		response.error("pinCode is mandatory parameter");
		return
	}

	var pinCode = parseInt(request.params.pinCode, 10);
	console.log("pinCode: " + pinCode);
	 // return;

	var receiptsQuery = new Parse.Query("Receipts");

	receiptsQuery.equalTo("pinCode", pinCode);
	receiptsQuery.first({
	    success: function(result) {
	    	if (result.get("state") != 1){
	    		response.error("receipt still not published");
	    		return;	
	    	}

	   	    response.success(result.get("receipt"));
       	},
	    error: function() {
	        response.error("receipt lookup failed");
	    }
	});
});

Parse.Cloud.define("associateInstallationWithUser", function(request, response) {
	if (request.params.installationId == undefined){
		response.error("installationId is mandatory parameter");
		return
	}

	if (request.user == undefined){
		response.error("no logged-in user");
		return
	}

	Parse.Cloud.useMasterKey();

	var installationsQuery = new Parse.Query(Parse.Installation);
	installationsQuery.equalTo("installationId", request.params.installationId);
	installationsQuery.first(
	{
  		success: function(object) 
  		{
    		object.set('user', request.user);
    		object.save();

    		var receiptsQuery = new Parse.Query("Receipts");
			receiptsQuery.equalTo("installationId", request.params.installationId);
			receiptsQuery.equalTo("user", null);
			receiptsQuery.each(function(receipt) 
  			{
  				receipt.set('user', request.user);
  				receipt.save();
  			}).then(function() 
  			{
  				response.success("");
			},
			function(error) 
			{
		        response.error("receipts lookup failed:" + error.message);
			});
       	},
	    error: function(error) {
	        response.error("receipts lookup failed:" + error.message);
		}
	});
});

Parse.Cloud.define("fetchReceiptsByInstallationId", function(request, response) {
	if (request.params.installationId == undefined){
		response.error("installationId is mandatory parameter");
		return
	}

	console.log(request.params.lastFetchTime);

	var receiptsQuery = new Parse.Query("Receipts");

	receiptsQuery.equalTo("installationId", request.params.installationId);
	receiptsQuery.equalTo("user", null);

	if (request.params.lastFetchTime != "" && request.params.lastFetchTime != undefined)
		receiptsQuery.greaterThan("createdAt", request.params.lastFetchTime);

	receiptsQuery.include("retailer");
	receiptsQuery.equalTo("state", 1);
	receiptsQuery.descending("createdAt");

	receiptsQuery.find({
	    success: function(results) {
	    	if (results.length == 0){
				response.success("No data");
				return;
	    	}

	   	    response.success(results);
       	},
	    error: function(error) {
	        response.error("receipts lookup failed:" + error.message);
	    }
	});
});

Parse.Cloud.define("fetchReceiptsByUser", function(request, response) {

	Parse.Cloud.useMasterKey();

	console.log(request.user);
	console.log(request.params.lastFetchTime);


	var receiptsQuery = new Parse.Query("Receipts");
	if (request.params.lastFetchTime != "" && request.params.lastFetchTime != undefined)
		receiptsQuery.greaterThan("createdAt", request.params.lastFetchTime);
	receiptsQuery.equalTo('user', request.user); 
	receiptsQuery.include("retailer");
	receiptsQuery.equalTo("state", 1);
	receiptsQuery.descending("createdAt");

	receiptsQuery.find({
	    success: function(results) {
	    	if (results.length == 0){
				response.success("No receipts found for user");
				return;
	    	}

	   	    response.success(results);
       	},
	    error: function(error) {
	        response.error("receipts lookup failed:" + error.message);
	    }
	});


/*
	var installationsQuery = new Parse.Query(Parse.Installation);
	installationsQuery.equalTo('user', request.user); 
	installationsQuery.select("installationId");
	installationsQuery.find({
		success: function(userInstallations) {
	    	if (userInstallations.length == 0){
				response.error("No installations for user");
				return;
	    	}

	    	//console.log(userInstallations);

			var receiptsQuery = new Parse.Query("Receipts");
			if (request.params.lastFetchTime != "" && request.params.lastFetchTime != undefined)
				receiptsQuery.greaterThan("createdAt", request.params.lastFetchTime);
			var installationIds = [];
			for (var i = 0; i < userInstallations.length; i++) { 
				installationIds.push(userInstallations[i].get("installationId"));
    		}

			console.log(installationIds);

			receiptsQuery.containedIn("installationId", installationIds);
			receiptsQuery.include("retailer");
			receiptsQuery.equalTo("state", 1);
			receiptsQuery.descending("createdAt");

			receiptsQuery.find({
			    success: function(results) {
			    	if (results.length == 0){
						response.success("No receipts found for user");
						return;
			    	}

			   	    response.success(results);
		       	},
			    error: function(error) {
			        response.error("receipts lookup failed:" + error.message);
			    }
			});
       	},
	    error: function(error) {
	        response.error("User installations lookup failed:" + error.message);
	    }
	});
*/
});

